package ru.petrushinda.centrifugo.client;

import static org.junit.jupiter.api.Assertions.assertTrue;

import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import ru.petrushinda.centrifugo.client.config.CentrifugoConfig;
import ru.petrushinda.centrifugo.client.model.draw.Draw;
import ru.petrushinda.centrifugo.client.model.draw.DrawMessage;

class CentrifugoClientImplTest {

  private static CentrifugoClient centrifugoClient;

  @BeforeAll
  static void init() {
    CentrifugoConfig centrifugoConfig = new CentrifugoConfig("127.0.0.1", 10000, "user:channel",
                                                             "public:channel");
    centrifugoClient = new CentrifugoClientImpl(centrifugoConfig);
  }

  @SneakyThrows
  @Test
  void publishUserMessage() {
    for (int i = 0; i < 100; i++) {
      var result = centrifugoClient.publishUserMessage(new DrawMessage(new Draw(i, 2)), "2");
      System.out.println("Publish user#" + i);
      Thread.sleep(1000L);
      assertTrue(result.isSuccess());
    }
  }

  @SneakyThrows
  @Test
  void publishPublicMessage() {
    for (int i = 0; i < 20; i++) {
      var result = centrifugoClient.publishPublicMessage(new DrawMessage(new Draw(i, 2)));
      System.out.println("Publish public#" + i);
      Thread.sleep(1000L);
      assertTrue(result.isSuccess());
    }
  }
}