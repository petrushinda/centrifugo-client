package ru.petrushinda.centrifugo.client;


import ru.petrushinda.centrifugo.client.model.PublishResult;

public interface CentrifugoClient {

  PublishResult publishUserMessage(Object data, String userId);

  PublishResult publishPublicMessage(Object data);
}
