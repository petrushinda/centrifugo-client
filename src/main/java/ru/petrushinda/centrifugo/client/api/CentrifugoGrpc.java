package ru.petrushinda.centrifugo.client.api;

import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

public final class CentrifugoGrpc {

  private CentrifugoGrpc() {}

  public static final String SERVICE_NAME = "api.Centrifugo";

  // Static method descriptors that strictly reflect the proto.
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static final io.grpc.MethodDescriptor<api.Api.PublishRequest,
      api.Api.PublishResponse> METHOD_PUBLISH =
      io.grpc.MethodDescriptor.<api.Api.PublishRequest, api.Api.PublishResponse>newBuilder()
          .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
          .setFullMethodName(generateFullMethodName(
              "api.Centrifugo", "Publish"))
          .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
              api.Api.PublishRequest.getDefaultInstance()))
          .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
              api.Api.PublishResponse.getDefaultInstance()))
          .build();
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static final io.grpc.MethodDescriptor<api.Api.BroadcastRequest,
      api.Api.BroadcastResponse> METHOD_BROADCAST =
      io.grpc.MethodDescriptor.<api.Api.BroadcastRequest, api.Api.BroadcastResponse>newBuilder()
          .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
          .setFullMethodName(generateFullMethodName(
              "api.Centrifugo", "Broadcast"))
          .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
              api.Api.BroadcastRequest.getDefaultInstance()))
          .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
              api.Api.BroadcastResponse.getDefaultInstance()))
          .build();
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static final io.grpc.MethodDescriptor<api.Api.UnsubscribeRequest,
      api.Api.UnsubscribeResponse> METHOD_UNSUBSCRIBE =
      io.grpc.MethodDescriptor.<api.Api.UnsubscribeRequest, api.Api.UnsubscribeResponse>newBuilder()
          .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
          .setFullMethodName(generateFullMethodName(
              "api.Centrifugo", "Unsubscribe"))
          .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
              api.Api.UnsubscribeRequest.getDefaultInstance()))
          .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
              api.Api.UnsubscribeResponse.getDefaultInstance()))
          .build();
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static final io.grpc.MethodDescriptor<api.Api.DisconnectRequest,
      api.Api.DisconnectResponse> METHOD_DISCONNECT =
      io.grpc.MethodDescriptor.<api.Api.DisconnectRequest, api.Api.DisconnectResponse>newBuilder()
          .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
          .setFullMethodName(generateFullMethodName(
              "api.Centrifugo", "Disconnect"))
          .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
              api.Api.DisconnectRequest.getDefaultInstance()))
          .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
              api.Api.DisconnectResponse.getDefaultInstance()))
          .build();
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static final io.grpc.MethodDescriptor<api.Api.PresenceRequest,
      api.Api.PresenceResponse> METHOD_PRESENCE =
      io.grpc.MethodDescriptor.<api.Api.PresenceRequest, api.Api.PresenceResponse>newBuilder()
          .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
          .setFullMethodName(generateFullMethodName(
              "api.Centrifugo", "Presence"))
          .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
              api.Api.PresenceRequest.getDefaultInstance()))
          .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
              api.Api.PresenceResponse.getDefaultInstance()))
          .build();
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static final io.grpc.MethodDescriptor<api.Api.PresenceStatsRequest,
      api.Api.PresenceStatsResponse> METHOD_PRESENCE_STATS =
      io.grpc.MethodDescriptor.<api.Api.PresenceStatsRequest, api.Api.PresenceStatsResponse>newBuilder()
          .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
          .setFullMethodName(generateFullMethodName(
              "api.Centrifugo", "PresenceStats"))
          .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
              api.Api.PresenceStatsRequest.getDefaultInstance()))
          .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
              api.Api.PresenceStatsResponse.getDefaultInstance()))
          .build();
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static final io.grpc.MethodDescriptor<api.Api.HistoryRequest,
      api.Api.HistoryResponse> METHOD_HISTORY =
      io.grpc.MethodDescriptor.<api.Api.HistoryRequest, api.Api.HistoryResponse>newBuilder()
          .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
          .setFullMethodName(generateFullMethodName(
              "api.Centrifugo", "History"))
          .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
              api.Api.HistoryRequest.getDefaultInstance()))
          .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
              api.Api.HistoryResponse.getDefaultInstance()))
          .build();
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static final io.grpc.MethodDescriptor<api.Api.HistoryRemoveRequest,
      api.Api.HistoryRemoveResponse> METHOD_HISTORY_REMOVE =
      io.grpc.MethodDescriptor.<api.Api.HistoryRemoveRequest, api.Api.HistoryRemoveResponse>newBuilder()
          .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
          .setFullMethodName(generateFullMethodName(
              "api.Centrifugo", "HistoryRemove"))
          .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
              api.Api.HistoryRemoveRequest.getDefaultInstance()))
          .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
              api.Api.HistoryRemoveResponse.getDefaultInstance()))
          .build();
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static final io.grpc.MethodDescriptor<api.Api.ChannelsRequest,
      api.Api.ChannelsResponse> METHOD_CHANNELS =
      io.grpc.MethodDescriptor.<api.Api.ChannelsRequest, api.Api.ChannelsResponse>newBuilder()
          .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
          .setFullMethodName(generateFullMethodName(
              "api.Centrifugo", "Channels"))
          .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
              api.Api.ChannelsRequest.getDefaultInstance()))
          .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
              api.Api.ChannelsResponse.getDefaultInstance()))
          .build();
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static final io.grpc.MethodDescriptor<api.Api.InfoRequest,
      api.Api.InfoResponse> METHOD_INFO =
      io.grpc.MethodDescriptor.<api.Api.InfoRequest, api.Api.InfoResponse>newBuilder()
          .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
          .setFullMethodName(generateFullMethodName(
              "api.Centrifugo", "Info"))
          .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
              api.Api.InfoRequest.getDefaultInstance()))
          .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
              api.Api.InfoResponse.getDefaultInstance()))
          .build();

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static CentrifugoStub newStub(io.grpc.Channel channel) {
    return new CentrifugoStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static CentrifugoBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new CentrifugoBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static CentrifugoFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new CentrifugoFutureStub(channel);
  }

  /**
   */
  public static abstract class CentrifugoImplBase implements io.grpc.BindableService {

    /**
     */
    public void publish(api.Api.PublishRequest request,
        io.grpc.stub.StreamObserver<api.Api.PublishResponse> responseObserver) {
      asyncUnimplementedUnaryCall(METHOD_PUBLISH, responseObserver);
    }

    /**
     */
    public void broadcast(api.Api.BroadcastRequest request,
        io.grpc.stub.StreamObserver<api.Api.BroadcastResponse> responseObserver) {
      asyncUnimplementedUnaryCall(METHOD_BROADCAST, responseObserver);
    }

    /**
     */
    public void unsubscribe(api.Api.UnsubscribeRequest request,
        io.grpc.stub.StreamObserver<api.Api.UnsubscribeResponse> responseObserver) {
      asyncUnimplementedUnaryCall(METHOD_UNSUBSCRIBE, responseObserver);
    }

    /**
     */
    public void disconnect(api.Api.DisconnectRequest request,
        io.grpc.stub.StreamObserver<api.Api.DisconnectResponse> responseObserver) {
      asyncUnimplementedUnaryCall(METHOD_DISCONNECT, responseObserver);
    }

    /**
     */
    public void presence(api.Api.PresenceRequest request,
        io.grpc.stub.StreamObserver<api.Api.PresenceResponse> responseObserver) {
      asyncUnimplementedUnaryCall(METHOD_PRESENCE, responseObserver);
    }

    /**
     */
    public void presenceStats(api.Api.PresenceStatsRequest request,
        io.grpc.stub.StreamObserver<api.Api.PresenceStatsResponse> responseObserver) {
      asyncUnimplementedUnaryCall(METHOD_PRESENCE_STATS, responseObserver);
    }

    /**
     */
    public void history(api.Api.HistoryRequest request,
        io.grpc.stub.StreamObserver<api.Api.HistoryResponse> responseObserver) {
      asyncUnimplementedUnaryCall(METHOD_HISTORY, responseObserver);
    }

    /**
     */
    public void historyRemove(api.Api.HistoryRemoveRequest request,
        io.grpc.stub.StreamObserver<api.Api.HistoryRemoveResponse> responseObserver) {
      asyncUnimplementedUnaryCall(METHOD_HISTORY_REMOVE, responseObserver);
    }

    /**
     */
    public void channels(api.Api.ChannelsRequest request,
        io.grpc.stub.StreamObserver<api.Api.ChannelsResponse> responseObserver) {
      asyncUnimplementedUnaryCall(METHOD_CHANNELS, responseObserver);
    }

    /**
     */
    public void info(api.Api.InfoRequest request,
        io.grpc.stub.StreamObserver<api.Api.InfoResponse> responseObserver) {
      asyncUnimplementedUnaryCall(METHOD_INFO, responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            METHOD_PUBLISH,
            asyncUnaryCall(
              new MethodHandlers<
                api.Api.PublishRequest,
                api.Api.PublishResponse>(
                  this, METHODID_PUBLISH)))
          .addMethod(
            METHOD_BROADCAST,
            asyncUnaryCall(
              new MethodHandlers<
                api.Api.BroadcastRequest,
                api.Api.BroadcastResponse>(
                  this, METHODID_BROADCAST)))
          .addMethod(
            METHOD_UNSUBSCRIBE,
            asyncUnaryCall(
              new MethodHandlers<
                api.Api.UnsubscribeRequest,
                api.Api.UnsubscribeResponse>(
                  this, METHODID_UNSUBSCRIBE)))
          .addMethod(
            METHOD_DISCONNECT,
            asyncUnaryCall(
              new MethodHandlers<
                api.Api.DisconnectRequest,
                api.Api.DisconnectResponse>(
                  this, METHODID_DISCONNECT)))
          .addMethod(
            METHOD_PRESENCE,
            asyncUnaryCall(
              new MethodHandlers<
                api.Api.PresenceRequest,
                api.Api.PresenceResponse>(
                  this, METHODID_PRESENCE)))
          .addMethod(
            METHOD_PRESENCE_STATS,
            asyncUnaryCall(
              new MethodHandlers<
                api.Api.PresenceStatsRequest,
                api.Api.PresenceStatsResponse>(
                  this, METHODID_PRESENCE_STATS)))
          .addMethod(
            METHOD_HISTORY,
            asyncUnaryCall(
              new MethodHandlers<
                api.Api.HistoryRequest,
                api.Api.HistoryResponse>(
                  this, METHODID_HISTORY)))
          .addMethod(
            METHOD_HISTORY_REMOVE,
            asyncUnaryCall(
              new MethodHandlers<
                api.Api.HistoryRemoveRequest,
                api.Api.HistoryRemoveResponse>(
                  this, METHODID_HISTORY_REMOVE)))
          .addMethod(
            METHOD_CHANNELS,
            asyncUnaryCall(
              new MethodHandlers<
                api.Api.ChannelsRequest,
                api.Api.ChannelsResponse>(
                  this, METHODID_CHANNELS)))
          .addMethod(
            METHOD_INFO,
            asyncUnaryCall(
              new MethodHandlers<
                api.Api.InfoRequest,
                api.Api.InfoResponse>(
                  this, METHODID_INFO)))
          .build();
    }
  }

  /**
   */
  public static final class CentrifugoStub extends io.grpc.stub.AbstractStub<CentrifugoStub> {
    private CentrifugoStub(io.grpc.Channel channel) {
      super(channel);
    }

    private CentrifugoStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected CentrifugoStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new CentrifugoStub(channel, callOptions);
    }

    /**
     */
    public void publish(api.Api.PublishRequest request,
        io.grpc.stub.StreamObserver<api.Api.PublishResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(METHOD_PUBLISH, getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void broadcast(api.Api.BroadcastRequest request,
        io.grpc.stub.StreamObserver<api.Api.BroadcastResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(METHOD_BROADCAST, getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void unsubscribe(api.Api.UnsubscribeRequest request,
        io.grpc.stub.StreamObserver<api.Api.UnsubscribeResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(METHOD_UNSUBSCRIBE, getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void disconnect(api.Api.DisconnectRequest request,
        io.grpc.stub.StreamObserver<api.Api.DisconnectResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(METHOD_DISCONNECT, getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void presence(api.Api.PresenceRequest request,
        io.grpc.stub.StreamObserver<api.Api.PresenceResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(METHOD_PRESENCE, getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void presenceStats(api.Api.PresenceStatsRequest request,
        io.grpc.stub.StreamObserver<api.Api.PresenceStatsResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(METHOD_PRESENCE_STATS, getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void history(api.Api.HistoryRequest request,
        io.grpc.stub.StreamObserver<api.Api.HistoryResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(METHOD_HISTORY, getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void historyRemove(api.Api.HistoryRemoveRequest request,
        io.grpc.stub.StreamObserver<api.Api.HistoryRemoveResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(METHOD_HISTORY_REMOVE, getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void channels(api.Api.ChannelsRequest request,
        io.grpc.stub.StreamObserver<api.Api.ChannelsResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(METHOD_CHANNELS, getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void info(api.Api.InfoRequest request,
        io.grpc.stub.StreamObserver<api.Api.InfoResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(METHOD_INFO, getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class CentrifugoBlockingStub extends io.grpc.stub.AbstractStub<CentrifugoBlockingStub> {
    private CentrifugoBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private CentrifugoBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected CentrifugoBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new CentrifugoBlockingStub(channel, callOptions);
    }

    /**
     */
    public api.Api.PublishResponse publish(api.Api.PublishRequest request) {
      return blockingUnaryCall(
          getChannel(), METHOD_PUBLISH, getCallOptions(), request);
    }

    /**
     */
    public api.Api.BroadcastResponse broadcast(api.Api.BroadcastRequest request) {
      return blockingUnaryCall(
          getChannel(), METHOD_BROADCAST, getCallOptions(), request);
    }

    /**
     */
    public api.Api.UnsubscribeResponse unsubscribe(api.Api.UnsubscribeRequest request) {
      return blockingUnaryCall(
          getChannel(), METHOD_UNSUBSCRIBE, getCallOptions(), request);
    }

    /**
     */
    public api.Api.DisconnectResponse disconnect(api.Api.DisconnectRequest request) {
      return blockingUnaryCall(
          getChannel(), METHOD_DISCONNECT, getCallOptions(), request);
    }

    /**
     */
    public api.Api.PresenceResponse presence(api.Api.PresenceRequest request) {
      return blockingUnaryCall(
          getChannel(), METHOD_PRESENCE, getCallOptions(), request);
    }

    /**
     */
    public api.Api.PresenceStatsResponse presenceStats(api.Api.PresenceStatsRequest request) {
      return blockingUnaryCall(
          getChannel(), METHOD_PRESENCE_STATS, getCallOptions(), request);
    }

    /**
     */
    public api.Api.HistoryResponse history(api.Api.HistoryRequest request) {
      return blockingUnaryCall(
          getChannel(), METHOD_HISTORY, getCallOptions(), request);
    }

    /**
     */
    public api.Api.HistoryRemoveResponse historyRemove(api.Api.HistoryRemoveRequest request) {
      return blockingUnaryCall(
          getChannel(), METHOD_HISTORY_REMOVE, getCallOptions(), request);
    }

    /**
     */
    public api.Api.ChannelsResponse channels(api.Api.ChannelsRequest request) {
      return blockingUnaryCall(
          getChannel(), METHOD_CHANNELS, getCallOptions(), request);
    }

    /**
     */
    public api.Api.InfoResponse info(api.Api.InfoRequest request) {
      return blockingUnaryCall(
          getChannel(), METHOD_INFO, getCallOptions(), request);
    }
  }

  /**
   */
  public static final class CentrifugoFutureStub extends io.grpc.stub.AbstractStub<CentrifugoFutureStub> {
    private CentrifugoFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private CentrifugoFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected CentrifugoFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new CentrifugoFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<api.Api.PublishResponse> publish(
        api.Api.PublishRequest request) {
      return futureUnaryCall(
          getChannel().newCall(METHOD_PUBLISH, getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<api.Api.BroadcastResponse> broadcast(
        api.Api.BroadcastRequest request) {
      return futureUnaryCall(
          getChannel().newCall(METHOD_BROADCAST, getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<api.Api.UnsubscribeResponse> unsubscribe(
        api.Api.UnsubscribeRequest request) {
      return futureUnaryCall(
          getChannel().newCall(METHOD_UNSUBSCRIBE, getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<api.Api.DisconnectResponse> disconnect(
        api.Api.DisconnectRequest request) {
      return futureUnaryCall(
          getChannel().newCall(METHOD_DISCONNECT, getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<api.Api.PresenceResponse> presence(
        api.Api.PresenceRequest request) {
      return futureUnaryCall(
          getChannel().newCall(METHOD_PRESENCE, getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<api.Api.PresenceStatsResponse> presenceStats(
        api.Api.PresenceStatsRequest request) {
      return futureUnaryCall(
          getChannel().newCall(METHOD_PRESENCE_STATS, getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<api.Api.HistoryResponse> history(
        api.Api.HistoryRequest request) {
      return futureUnaryCall(
          getChannel().newCall(METHOD_HISTORY, getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<api.Api.HistoryRemoveResponse> historyRemove(
        api.Api.HistoryRemoveRequest request) {
      return futureUnaryCall(
          getChannel().newCall(METHOD_HISTORY_REMOVE, getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<api.Api.ChannelsResponse> channels(
        api.Api.ChannelsRequest request) {
      return futureUnaryCall(
          getChannel().newCall(METHOD_CHANNELS, getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<api.Api.InfoResponse> info(
        api.Api.InfoRequest request) {
      return futureUnaryCall(
          getChannel().newCall(METHOD_INFO, getCallOptions()), request);
    }
  }

  private static final int METHODID_PUBLISH = 0;
  private static final int METHODID_BROADCAST = 1;
  private static final int METHODID_UNSUBSCRIBE = 2;
  private static final int METHODID_DISCONNECT = 3;
  private static final int METHODID_PRESENCE = 4;
  private static final int METHODID_PRESENCE_STATS = 5;
  private static final int METHODID_HISTORY = 6;
  private static final int METHODID_HISTORY_REMOVE = 7;
  private static final int METHODID_CHANNELS = 8;
  private static final int METHODID_INFO = 9;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final CentrifugoImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(CentrifugoImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_PUBLISH:
          serviceImpl.publish((api.Api.PublishRequest) request,
              (io.grpc.stub.StreamObserver<api.Api.PublishResponse>) responseObserver);
          break;
        case METHODID_BROADCAST:
          serviceImpl.broadcast((api.Api.BroadcastRequest) request,
              (io.grpc.stub.StreamObserver<api.Api.BroadcastResponse>) responseObserver);
          break;
        case METHODID_UNSUBSCRIBE:
          serviceImpl.unsubscribe((api.Api.UnsubscribeRequest) request,
              (io.grpc.stub.StreamObserver<api.Api.UnsubscribeResponse>) responseObserver);
          break;
        case METHODID_DISCONNECT:
          serviceImpl.disconnect((api.Api.DisconnectRequest) request,
              (io.grpc.stub.StreamObserver<api.Api.DisconnectResponse>) responseObserver);
          break;
        case METHODID_PRESENCE:
          serviceImpl.presence((api.Api.PresenceRequest) request,
              (io.grpc.stub.StreamObserver<api.Api.PresenceResponse>) responseObserver);
          break;
        case METHODID_PRESENCE_STATS:
          serviceImpl.presenceStats((api.Api.PresenceStatsRequest) request,
              (io.grpc.stub.StreamObserver<api.Api.PresenceStatsResponse>) responseObserver);
          break;
        case METHODID_HISTORY:
          serviceImpl.history((api.Api.HistoryRequest) request,
              (io.grpc.stub.StreamObserver<api.Api.HistoryResponse>) responseObserver);
          break;
        case METHODID_HISTORY_REMOVE:
          serviceImpl.historyRemove((api.Api.HistoryRemoveRequest) request,
              (io.grpc.stub.StreamObserver<api.Api.HistoryRemoveResponse>) responseObserver);
          break;
        case METHODID_CHANNELS:
          serviceImpl.channels((api.Api.ChannelsRequest) request,
              (io.grpc.stub.StreamObserver<api.Api.ChannelsResponse>) responseObserver);
          break;
        case METHODID_INFO:
          serviceImpl.info((api.Api.InfoRequest) request,
              (io.grpc.stub.StreamObserver<api.Api.InfoResponse>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static final class CentrifugoDescriptorSupplier implements io.grpc.protobuf.ProtoFileDescriptorSupplier {
    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return api.Api.getDescriptor();
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (CentrifugoGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new CentrifugoDescriptorSupplier())
              .addMethod(METHOD_PUBLISH)
              .addMethod(METHOD_BROADCAST)
              .addMethod(METHOD_UNSUBSCRIBE)
              .addMethod(METHOD_DISCONNECT)
              .addMethod(METHOD_PRESENCE)
              .addMethod(METHOD_PRESENCE_STATS)
              .addMethod(METHOD_HISTORY)
              .addMethod(METHOD_HISTORY_REMOVE)
              .addMethod(METHOD_CHANNELS)
              .addMethod(METHOD_INFO)
              .build();
        }
      }
    }
    return result;
  }
}
