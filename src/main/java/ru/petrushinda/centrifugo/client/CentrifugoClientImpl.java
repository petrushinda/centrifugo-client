package ru.petrushinda.centrifugo.client;

import api.Api.PublishRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.protobuf.ByteString;
import io.grpc.CallCredentials;
import io.grpc.Channel;
import io.grpc.Grpc;
import io.grpc.InsecureChannelCredentials;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import ru.petrushinda.centrifugo.client.api.CentrifugoGrpc;
import ru.petrushinda.centrifugo.client.api.CentrifugoGrpc.CentrifugoBlockingStub;
import ru.petrushinda.centrifugo.client.config.CentrifugoConfig;
import ru.petrushinda.centrifugo.client.model.PublishResult;

@Slf4j
public class CentrifugoClientImpl implements CentrifugoClient {

  private final static ObjectMapper OBJECT_MAPPER = new ObjectMapper();

  private final CentrifugoBlockingStub stub;
  private final CentrifugoConfig centrifugoConfig;

  public CentrifugoClientImpl(CentrifugoConfig centrifugoConfig) {
    Channel channel = Grpc.newChannelBuilderForAddress(centrifugoConfig.getHost(), centrifugoConfig.getPort(),
                                                       InsecureChannelCredentials.create()).build();
    this.stub = CentrifugoGrpc.newBlockingStub(channel);
    this.centrifugoConfig = centrifugoConfig;
  }

  @Override
  public PublishResult publishUserMessage(Object data, String userId) {
    return publish(data, String.join("#", centrifugoConfig.getUserChannel(), userId));
  }

  @Override
  public PublishResult publishPublicMessage(Object data) {
    return publish(data, centrifugoConfig.getPublicChannel());
  }

  @SneakyThrows
  private PublishResult publish(Object data, String channel) {
    var response = stub.publish(PublishRequest.newBuilder()
                                    .setChannel(channel)
                                    .setData(ByteString.copyFrom(OBJECT_MAPPER.writeValueAsBytes(data)))
                                    .build());
    if (response.hasError()) {
      String error = response.getError().getMessage();
      log.warn(error);
      return PublishResult.error(error);
    } else {
      return PublishResult.success();
    }
  }
}
