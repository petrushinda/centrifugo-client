package ru.petrushinda.centrifugo.client.config;

import lombok.Data;

@Data
public class CentrifugoConfig {
  private final String host;
  private final int port;
  private final String userChannel;
  private final String publicChannel;
}
