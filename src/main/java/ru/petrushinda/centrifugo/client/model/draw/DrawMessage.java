package ru.petrushinda.centrifugo.client.model.draw;

import ru.petrushinda.centrifugo.client.model.PublishMessage;

public class DrawMessage extends PublishMessage<Draw> {

  public DrawMessage(Draw data) {
    super("draw", data);
  }
}
