package ru.petrushinda.centrifugo.client.model.draw;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Draw {
  private long id;
  private long userId;
}
