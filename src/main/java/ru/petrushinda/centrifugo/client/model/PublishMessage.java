package ru.petrushinda.centrifugo.client.model;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Data;
import ru.petrushinda.centrifugo.client.model.draw.DrawMessage;

@Data
@JsonTypeInfo(
    use = JsonTypeInfo.Id.NAME,
    include = JsonTypeInfo.As.PROPERTY,
    property = "type")
@JsonSubTypes({
                  @Type(name = "draw", value = DrawMessage.class)
              })
public abstract class PublishMessage<T> {

  private String type;
  private T data;

  protected PublishMessage(String type, T data) {
    this.type = type;
    this.data = data;
  }
}
