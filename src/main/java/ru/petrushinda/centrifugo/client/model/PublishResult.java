package ru.petrushinda.centrifugo.client.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class PublishResult {

  private final boolean success;
  private final String error;

  public static PublishResult success() {
    return new PublishResult(true, null);
  }

  public static PublishResult error(String error) {
    return new PublishResult(false, error);
  }
}
